emergent_reasons
BCHN janitor

emergent_reasons has held many roles professionally from software
developer to teacher to president. In addition to being a Bitcoin
and later Bitcoin Cash investor, the block size conflict that led
to the creation of Bitcoin Cash convinced him that it was important
to take an active role in building Bitcoin, helping it to fulfill
its potential as the best money in the world. Since then, he has
done what he can to help many projects and build new industry
on Bitcoin Cash. He sees Bitcoin Cash Node as a critical step
in Bitcoin's maturation from high-risk dependence on a reference
node into a true cooperative and competitive node landscape.
