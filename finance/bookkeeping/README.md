# BCHN Bookkeeping

"Where the crypto rubber is trying to get a grip on the Road of Finance..."


## Overview

We use [plain text accounting](https://plaintextaccounting.org) tools
and open source wallet software, all our transactions are on the Bitcoin
Cash blockchain.

The file format is pretty standard, and we should avoid using any features
that are not commonly supported by other 'ledger-like' tools.

The bookkeeping is not fully automated yet, the journals are updated manually
to produce reports, but complete journal data is to be stored here so that
it can be audited against the blockchain.


## File descriptions

- bchn.journal : top-level accounts file, processed using [hledger](https://hledger.org/) or [ledger](https://www.ledger-cli.org/)
- prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk.journal - main wallet ledger,
  included by the top-level journal
- electroncash_export.csv.rules : CSV import rules for hledger to process
  Electron Cash CSV wallet export
- pp03e95qz7yene7v4vdx2vfsqx4qsq4wfvw8z3pkjf.journal
- prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk_replays_to_abc.journal -
  journal of donations to the "ABC" fork of the wallet. Mostly replays.


## Installing the `ledger` tool:

It is available as a package called `ledger` on Debian Linux, and probably
most other up to date Linux distributions.


## Running the ledger tool

To get a balance:

`ledger -f bchn.journal balance`

## Notes

Electron Cash wallet CSV files need to be pre-processed to transform '--'
fee values into zeroes, otherwise hledger throws an error while converting
the files.

